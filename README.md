# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Proyecto creado como Proyecto Final de Ciclo para el Instituto Tecnológico de Telefónica.

Version 1.0

### How do I get set up? ###

El repositorio incluye dos carpetas con el instalador MSI del Launcher y el ejecutable EXE del videojuego.

Enlace del repositorio con el código del videojuego: 

* https://github.com/DanielMtezFdez/CocheMania

Enlace del repositorio con el código del launcher: 

* https://github.com/Idrill7/LauncherAnakonda_TFG

### Who do I talk to? ###

GitHub de los creadores:

* https://github.com/DanielMtezFdez

* https://github.com/Idrill7